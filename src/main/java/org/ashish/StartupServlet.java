package org.ashish;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StartupServlet
 */
@WebServlet(
		name = "/StartupServlet",
		urlPatterns = {"/submit"}
		)
public class StartupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public StartupServlet() {
		AwsUtil.setAWSCred();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Inside Do GET");
		String emailId = request.getParameter("email");
		UserObj user = null;
		if(emailId!=null && !emailId.trim().isEmpty()){
			DatabaseQueries dbQuery = new DatabaseQueries();
			user = dbQuery.getUserDataFromDB(emailId);
			if(user!=null && user.getProfileMetaId()!=null && user.getResumeMetaId()!=null){
				//fetch it from amazon s3// 
				//S3Operations s3Operations = new S3Operations();
				//s3Operations.downloadFile("profile", "profile.ping", user.getProfileMetaId());
				// store in the tmp location with retention policy
			}
		}
		request.setAttribute("user", user);
		request.getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
